#include <stdio.h>


#define IRX_DEFINE(mod) \
extern unsigned char mod##_irx[]; \
extern unsigned int size_##mod##_irx

#define IRX_LOAD(mod) \
    if (SifExecModuleBuffer(mod##_irx, size_##mod##_irx, 0, NULL, NULL) < 0) \
        printf("Could not load ##mod##\n")


IRX_DEFINE(ppc_trigger);


int main(int argc, char *argv[])
{
	printf("IOP-PPC test\n");

	SifInitRpc(0);
	SifLoadFileInit();

	IRX_LOAD(ppc_trigger);

	while(1);

	return 0;
}
