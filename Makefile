all:
	$(MAKE) -C iop-ppc all
	$(MAKE) -C iop all
	$(MAKE) -C ee all

clean:
	$(MAKE) -C iop-ppc clean
	$(MAKE) -C iop clean
	$(MAKE) -C ee clean

debug:
	$(MAKE) -C iop-ppc debug

run:
	$(MAKE) -C ee run
